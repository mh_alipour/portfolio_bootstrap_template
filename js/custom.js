$(document).ready(function() {

    /* Progress-bar
    -----------------------------------------------*/
    if ($(".meter > span").length > 0) {
        $(".meter > span").each(function() {
            $(this)
                .data("origWidth", $(this).width())
                .width(0)
                .animate({
                    width: $(this).data("origWidth") // or + "%" if fluid
                }, 1200);
        });
    }

    /* Wizard
    -----------------------------------------------*/
    if ($('#js-wizard-form').length > 0) {
        let value = 40;

        $("#js-wizard-form").bootstrapWizard({
            'tabClass': 'nav-tab',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-back',
            'lastSelector': '.btn-last',
            'onNext': function(tab, navigation, index) {
                if (value === 100) return;
                const progressBar = $('#js-progress').find('.progress-bar');
                const current = index + 1;
                if (current > 1) {
                    value += 30;
                    progressBar.css('width', value + '%');
                }

            },
            'onPrevious': function(tab, navigation, index) {
                if (value === 0) return;
                const progressBar = $('#js-progress').find('.progress-bar');
                const current = index + 1;
                if (current !== 0) {
                    value -= 30;
                    progressBar.css('width', value + '%');
                }

            }

        });
    }


    /* Smoothscroll js
    -----------------------------------------------*/
    $(function() {
        $('.navbar-default a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 49
            }, 1000);
            event.preventDefault();
        });
    });


    /* Back to Top
    -----------------------------------------------*/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
    // Animate the scroll to top
    $('.go-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 300);
    });


    /* wow
    -------------------------------*/
    new WOW({ mobile: false }).init();


    /* bxSlider
    -------------------------------*/
    if ($('.bxslider').length > 0) {
        $('.bxslider').bxSlider({
            adaptiveHeight: true,
            controls: false,
            auto: true,
            mode: 'fade',
            pager: false,
        });
    }


    /* Isotope
       -------------------------------*/
    if ($('.iso-box-wrapper').length > 0) {
        var $container = $('.iso-box-wrapper'),
            $imgs = $('.iso-box img');

        $container.isotope({
            itemSelector: '.iso-box',
            layoutMode: 'fitRows',
        });

        $imgs.on(function() {
            $container.isotope('reLayout');
        });

        //filter items on button click

        $('.filter-wrapper li a').click(function() {
            var $this = $(this),
                filterValue = $this.attr('data-filter');

            $container.isotope({
                filter: filterValue,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });

            // don't proceed if already selected 

            if ($this.hasClass('selected')) {
                return false;
            }

            var filter_wrapper = $this.closest('.filter-wrapper');
            filter_wrapper.find('.selected').removeClass('selected');
            $this.addClass('selected');

            return false;
        });
    }

});